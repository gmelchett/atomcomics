# AtomComics - Converts .cbz to Atom feed

I constantly forgot that I have bought some HumbleBundle comics. And since I'm
a fan of RSS/Atom feeds, this is a small program that can convert .cbz files to
atom feeds.
The comic pages might get downscaled and converted to `webp`. (`webp` is way
way more effective than `jpeg`.) Each page gets a different time stamp, first
page gets becomes the oldest page.


## Installation

1. Clone this git
2. go get
3. go build
4. Copy the output binary `atomcomics` somewhere convenient.

## Usage
```
Usage of ./atomcomics:
  -i string
        cbz input
  -r string
        Root URL where feed will be located
  -t string
        Feed name
  -w int
        With of comics (Will only be down scaled) (default 1024)
```

A random directory name will be added to the given URL. It makes it somewhat
less likely that someone will copy your comics.


## Licence

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
