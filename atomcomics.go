/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2020
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package main

import (
	"archive/zip"
	"bytes"
	"encoding/hex"
	"flag"
	"fmt"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/chai2010/webp"
	"github.com/disintegration/imaging"
	"github.com/gorilla/feeds"
)

const SAVE_CURSOR_POS = "\0337"
const RESTORE_CURSOR_POS = "\0338"

func main() {

	var cbzFile, title, rootUrl string
	var width int

	flag.StringVar(&cbzFile, "i", "", "cbz input")
	flag.StringVar(&title, "t", "", "Feed name")
	flag.StringVar(&rootUrl, "r", "", "Root URL where feed will be located")
	flag.IntVar(&width, "w", 1024, "With of comics (Will only be down scaled)")

	flag.Parse()

	if cbzFile == "" {
		log.Fatalf("No input file given.")
	}

	if title == "" {
		log.Fatalf("No title given.")
	}

	if rootUrl == "" {
		log.Fatalf("No root URL given.")
	}

	r, err := zip.OpenReader(cbzFile)
	if err != nil {
		log.Fatal("Failed to open provided file as cbz", err)
	}
	defer r.Close()

	fileNames := make([]string, 0, len(r.File))

	now := time.Now()
	rand.Seed(now.UnixNano())
	randBytes := make([]byte, 16)
	rand.Read(randBytes)
	randDir := hex.EncodeToString(randBytes)

	os.MkdirAll(randDir, 0755)

	fmt.Printf("Converting images: [" + SAVE_CURSOR_POS + strings.Repeat(".", len(r.File)) + "]" + RESTORE_CURSOR_POS)

	for _, f := range r.File {
		if f.FileHeader.FileInfo().IsDir() {
			continue
		}

		ext := filepath.Ext(strings.ToLower(f.Name))
		if ext != ".jpg" && ext != ".webp" && ext != ".png" && ext != ".gif" {
			continue
		}

		rc, err := f.Open()
		if err != nil {
			log.Fatalf("Error opening %s inside %s error: %s\n", cbzFile, f.Name, err)
		}
		img, _, err := image.Decode(rc)
		if err != nil {
			log.Fatalf("Can't decode image: %s\n", f.Name)
		}
		rc.Close()

		var newImg image.Image
		bounds := img.Bounds()

		if bounds.Max.X > width {

			height := int(float64(width) * float64(bounds.Max.Y) / float64(bounds.Max.X))
			fmt.Printf(SAVE_CURSOR_POS + "o" + RESTORE_CURSOR_POS)

			newImg = imaging.Resize(img, width, height, imaging.Lanczos)
		} else {
			newImg = img
		}

		var buf bytes.Buffer
		if err = webp.Encode(&buf, newImg, &webp.Options{Lossless: false, Quality: 70, Exact: false}); err != nil {
			log.Fatalf("webp encode failed with: %s\n", err)
		}

		outFileName := strings.Replace(filepath.Base(strings.ToLower(f.Name)), ".jpg", ".webp", -1)

		if err = ioutil.WriteFile(filepath.Join(randDir, outFileName), buf.Bytes(), 0644); err != nil {
			log.Fatalf("Failed to write image to disk: %s\n", err)
		}

		fileNames = append(fileNames, outFileName)
		fmt.Printf("O")
	}

	feed := &feeds.Feed{
		Title:   title,
		Link:    &feeds.Link{},
		Author:  &feeds.Author{},
		Created: now,
	}

	atomFile := filepath.Join(randDir, "feed.atom")

	for i := 0; i < len(fileNames); i++ {
		feed.Add(&feeds.Item{
			Title:   fmt.Sprintf("Page %d of %d", i+1, len(fileNames)),
			Link:    &feeds.Link{},
			Created: time.Now().AddDate(0, 0, -2).Add(time.Minute * time.Duration(i)),
			Content: fmt.Sprintf("<img src=\"%s\">", rootUrl+"/"+randDir+"/"+fileNames[i]),
		})
	}

	if file, err := os.OpenFile(atomFile, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644); err == nil {
		feed.WriteAtom(file)
		file.Close()
	} else {
		log.Fatalf("Failed to save: %s error:%s", atomFile, err)
	}
	fmt.Printf("\nCopy the directory %s/ and its contains to your server and add %s to your feed reader\n", randDir, rootUrl+atomFile)
}
